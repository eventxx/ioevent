/*
 * Embedded build:
 * c++ -I.. -o ev++-test-eof -DEV_STANDALONE=1 -DEV_MULTIPLICITY=0 ev++-test-eof.cpp ../ev.c
 *  or:
 * c++ -I.. -o ev++-test-eof -DEV_STANDALONE=1 ev++-test-eof.cpp ../ev.c
 *
 *
 * Wed 2006-12-27 - Modified by Leandro Lucarella <llucax+eventxx@gmail.com>
 *
 *     Adapted to test the C++ inteface.
 *
 * Wed 2007-12-13 - Modified by Chris Brody <chris.brody@gmail.com>
 *
 *     Adapted to test ev++.h with the default ev_loop().
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cerrno>

#include <ev++.h>

int test_okay = 1;
int called = 0;

void
read_cb_func(ev::io & io, int)
{
	char buf[256];
	int len;

	int fd = io.fd;

	len = read(fd, buf, sizeof(buf));

	printf("%s: read %d%s\n", __func__,
	    len, len ? "" : " - means EOF");

	if (len == 0) {
		io.stop();

		if (called == 1)
			test_okay = 0;
	}

	called++;
}

int
main (int argc, char **argv)
{
	const char* test = "test string";
	int pair[2];

	ev_default_loop(0);

	if (socketpair(AF_UNIX, SOCK_STREAM, 0, pair) == -1)
		return (1);

	write(pair[0], test, strlen(test)+1);
	shutdown(pair[0], SHUT_WR);

	/* Initalize event */
	ev::io * ie = new ev::io();

	ie->set<&read_cb_func>(NULL);

	ie->start(pair[1], EV_READ); /* NOTE: running until stop() is called */

	ev_loop (EV_DEFAULT_ 0);

	delete ie;

	return (test_okay);
}

