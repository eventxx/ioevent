/*
 * Embedded build:
 * c++ -I.. -o io++-time-test -DEV_STANDALONE=1 -DEV_MULTIPLICITY=0 io++-time-test.cpp ../ev.c
 *  or:
 * c++ -I.. -o io++-time-test -DEV_STANDALONE=1 io++-time-test.cpp ../ev.c
 *
 *
 * Wed 2007-12-14 - Modified by Chris Brody <chris.brody@gmail.com>
 *
 *     Adapted to test io++.h with periodic repeat.
 *
 */


#include <sys/types.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/stat.h>
#ifndef WIN32
#include <sys/queue.h>
#include <unistd.h>
#endif
#include <time.h>
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <io++.h>

int lasttime;

void
timeout_cb(int, short, void *)
{
	int newtime = time(NULL);

	printf("%s: called at %d: %d\n", __func__, newtime,
	    newtime - lasttime);

	lasttime = newtime;
}

int
main (int argc, char **argv)
{
	/* Init default loop */
	ev_default_loop(0);

	/* Init timer */
	evx::itimer timeout;
	timeout.set<&timeout_cb>(NULL);
 
	timeout.start(2, 2);

	lasttime = time(NULL);
	
	ev_loop (EV_DEFAULT_ 0);

	return (0);
}

