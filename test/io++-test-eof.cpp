/*
 * Embedded build:
 * c++ -I.. -o io++-test-eof -DEV_STANDALONE=1 -DEV_MULTIPLICITY=0 io++-test-eof.cpp ../ev.c
 *  or:
 * c++ -I.. -o io++-test-eof -DEV_STANDALONE=1 io++-test-eof.cpp ../ev.c
 *
 *
 * Wed 2006-12-27 - Modified by Leandro Lucarella <llucax+eventxx@gmail.com>
 *
 *     Adapted to test the C++ inteface.
 *
 * Wed 2007-12-14 - Modified by Chris Brody <chris.brody@gmail.com>
 *
 *     Adapted to test io++.h with the default ev_loop().
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cerrno>

#include <io++.h>

int test_okay = 1;
int called = 0;

evx::io * ie;

void
read_cb(int fd, short event, void *arg)
{
	char buf[256];
	int len;

	len = read(fd, buf, sizeof(buf));

	printf("%s: read %d%s\n", __func__,
	    len, len ? "" : " - means EOF");

	if (len == 0) {
		ie->stop();

		if (called == 1)
			test_okay = 0;
	}

	called++;
}

int
main (int argc, char **argv)
{
	const char* test = "test string";
	int pair[2];

	ev_default_loop(0);

	if (socketpair(AF_UNIX, SOCK_STREAM, 0, pair) == -1)
		return (1);

	write(pair[0], test, strlen(test)+1);
	shutdown(pair[0], SHUT_WR);

	/* Initalize event */
	ie = new evx::io();

	ie->set<&read_cb>(NULL);

	ie->start(pair[1], EV_READ);

	ev_loop (EV_DEFAULT_ 0);

	delete ie;

	return (test_okay);
}

