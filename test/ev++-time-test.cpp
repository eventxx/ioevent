/*
 * Embedded build:
 * c++ -I.. -o ev++-time-test -DEV_STANDALONE=1 -DEV_MULTIPLICITY=0 ev++-time-test.cpp ../ev.c
 *  or:
 * c++ -I.. -o ev++-time-test -DEV_STANDALONE=1 ev++-time-test.cpp ../ev.c
 *
 *
 * Wed 2007-12-12 - Modified by Chris Brody <chris.brody@gmail.com>
 *
 *     Adapted to test C++ inteface.
 *
 */


#include <sys/types.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/stat.h>
#ifndef WIN32
#include <sys/queue.h>
#include <unistd.h>
#endif
#include <time.h>
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <ev++.h>

int lasttime;

void
timeout_cb_func(ev::timer & timeout, int)
{
	int newtime = time(NULL);

	printf("%s: called at %d: %d\n", __func__, newtime,
	    newtime - lasttime);

	lasttime = newtime;

	timeout.start(2); /* NOTE: implicit conversion ==> double */
}

int
main (int argc, char **argv)
{
	/* Init default loop */
	ev_default_loop(0);

	/* Init timer */
	ev::timer timeout;
	timeout.set<&timeout_cb_func>(NULL);
 
	timeout.start(2); /* NOTE: implicit conversion ==> double */

	lasttime = time(NULL);
	
	ev_loop (EV_DEFAULT_ 0);

	return (0);
}

