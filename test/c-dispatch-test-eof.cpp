/*
 * Compile with:
 * c++ -I/usr/local/include -o c-dispatch-test-eof c-dispatch-test-eof.cpp -L/usr/local/lib -levent
 *
 *
 * Wed 2006-12-27 - Modified by Leandro Lucarella <llucax+eventxx@gmail.com>
 *
 *     Adapted to test the C++ inteface.
 *
 * Wed 2007-12-12 - Modified by Chris Brody <chris.brody@gmail.com>
 *
 *     Adapted to test the C++ inteface with default C event_dispatch().
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cerrno>

#include <eventxx>

int test_okay = 1;
int called = 0;

eventxx::cevent* ev;

void
read_cb(int fd, short event, void *arg)
{
	char buf[256];
	int len;

	len = read(fd, buf, sizeof(buf));

	printf("%s: read %d%s\n", __func__,
	    len, len ? "" : " - means EOF");

	if (len) {
		if (!called)
			::event_add(ev, NULL);
	} else if (called == 1)
		test_okay = 0;

	called++;
}

int
main (int argc, char **argv)
{
	const char* test = "test string";
	int pair[2];

	::event_init();

	if (socketpair(AF_UNIX, SOCK_STREAM, 0, pair) == -1)
		return (1);

	write(pair[0], test, strlen(test)+1);
	shutdown(pair[0], SHUT_WR);

	/* Initalize one event */
	ev = new eventxx::cevent(pair[1], eventxx::READ, read_cb, NULL);

	::event_add(ev, NULL);

	::event_dispatch();

	delete ev;

	return (test_okay);
}

