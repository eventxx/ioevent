#ifndef _IOPP__H_
#define _IOPP__H_

#include <ev++.h>

namespace evx {

typedef void cbtype (int, short, void *);

struct io : public ev::io
{
	template<cbtype * cbfunc>
	void set(void * data = 0)
	{
		set_(data, cbthunk<cbfunc>);
	}

	template<cbtype * cbfunc>
	static void cbthunk(EV_P_ ev_io * w, int e)
	{
		(*cbfunc)(w->fd, e, w->data);
	}
};

struct itimer : public ev::timer
{
	template<cbtype * cbfunc>
	void set(void * data = 0)
	{
		set_(data, cbthunk<cbfunc>);
	}

	template<cbtype * cbfunc>
	static void cbthunk(EV_P_ ev_timer * w, int e)
	{
		(*cbfunc)(-1, 0, w->data);
	}

	void set(int after, int repeat = 0)
	{
		set((ev_tstamp)after, (ev_tstamp)repeat);
	}

	void start(int after, int repeat = 0)
	{
		//Apparent stack problem:
		//start((ev_tstamp)after, (ev_tstamp)repeat);
		//Required workaround:
		ev::timer::start((ev_tstamp)after, (ev_tstamp)repeat);
	}
};

}

#endif
