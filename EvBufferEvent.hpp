#ifndef _EV_BUFFER_EVENT__HPP_
#define _EV_BUFFER_EVENT__HPP_

#include <EvBufferEventInternal.hpp>


struct EvBufferEvent : EvBufferEventInternal
{

	/// C read/write callback function used in the C evbuffer API.
	typedef void (*readwritecb)(struct bufferevent *, void *);

	/// C error callback function used in the C evbuffer API.
	typedef void (*errorcb)(struct bufferevent *, short, void *);

	// TODO: EvBufferEvent(int fd, ...) throw(...) { ... }

	// Future: to be deprecated by EvBufferEvent(int fd, ...) throw(...)
	/* public: for now */
		/**
		 * Constructor to setup callbacks
		 */
		EvBufferEvent(readwritecb readcb, readwritecb writecb, errorcb errcb, void * arg)
		{
			_readcb = readcb;
			_writecb = writecb;
			_errorcb = errcb;

			_arg = arg;
		}

	// Future: to be deprecated by EvBufferEvent(...) throw(...)
	/* public: for now */
		/**
		 * Open the bufferevent object with the file/socket descriptor
		 */
		void open(int fd) /* throw(...) */
		{
			(void)_bufferevent_new(fd);
		}

		/**
		 * Open the bufferevent object by accepting a connection on the socket descriptor
		 */
		void accept(int listen_sock) /* throw(...) */
		{
			(void)_bufev_socket_accept(listen_sock);
		}

	/* public: */

		/**
		 * Write data to the buffer.
		 *
		 * @param buf Buffer of data to be written.
		 *
		 * @param len Length of data to be written.
		 *
		 */
		int write(const void * buf, int len)
		{
			return _write((char *)buf, len);
		}

		/**
		 * Read data from the buffer.
		 *
		 * @param buf Buffer to store data.
		 *
		 * @param len Size of buffer to store data.
		 *
		 */
		int read(char * buf, int len)
		{
			return _read(buf, len);
		}

	/* protected: */
		/* virtual */
		void _readcb_handler()
		{
			(*_readcb)(_bufferevent, _arg);
		}

		/* virtual */
		void _writecb_handler()
		{
			(*_writecb)(_bufferevent, _arg);
		}

		/* virtual */
		void _errorcb_handler(short what)
		{
			(*_errorcb)(_bufferevent, what, _arg);
		}

	protected:
		EvBufferEvent() { }

	/* private: */
		readwritecb _readcb, _writecb;
		errorcb _errorcb;
		void * _arg;
};

#endif // _EV_BUFFER_EVENT__HPP_

// vim: set filetype=cpp :
