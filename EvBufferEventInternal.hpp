#ifndef _EV_BUFFER_EVENT_INTERNAL__HPP_
#define _EV_BUFFER_EVENT_INTERNAL__HPP_

#include <event.h>


struct EvBufferEventInternal
{

	/* protected: */
		/**
		 * Constructor to setup callbacks
		 */
		EvBufferEventInternal()
		{
			_bufferevent = NULL;
		}

	/* protected: */
		/**
		 * Open the bufferevent object with the file/socket descriptor
		 * by calling ::bufferevent_new()
		 */
		struct bufferevent * _bufferevent_new(int fd) /* throw(...) */
		{
			return _bufferevent =
				::bufferevent_new(fd, _readcb_wrapper, _writecb_wrapper, _errorcb_wrapper,
					reinterpret_cast< void * >(this));
		}

		/**
		 * Open the bufferevent object with the listen socket descriptor
		 * by calling ::bufev_socket_accept()
		 */
		struct bufferevent * _bufev_socket_accept(int listen_sock) /* throw(...) */
		{
			return _bufferevent =
				::bufev_socket_accept(listen_sock, _readcb_wrapper, _writecb_wrapper, _errorcb_wrapper,
					reinterpret_cast< void * >(this));
		}

	/* TODO (future): protected: */
		/**
		 * Enable certain bufferevent callbacks by calling ::bufferevent_enable()
		 */
		int _enable(short ev)
		{
			return ::bufferevent_enable(_bufferevent, ev);
		}

		/**
		 * Disable certain bufferevent callbacks by calling ::bufferevent_disable()
		 */
		int _disable(short ev)
		{
			return ::bufferevent_disable(_bufferevent, ev);
		}

		int _write(char * buf, int len)
		{
			return ::bufferevent_write(_bufferevent, buf, len);
		}

		int _read(char * buf, int len)
		{
			return ::bufferevent_read(_bufferevent, buf, len);
		}

		int _input_length()
		{
			return EVBUFFER_LENGTH(_bufferevent->input);
		}
		int _output_length()
		{
			return EVBUFFER_LENGTH(_bufferevent->output);
		}

	/* public: */
		virtual ~EvBufferEventInternal()
		{
			if (_bufferevent != NULL)
				::bufferevent_free(_bufferevent);
		}

	/* protected: */
		struct bufferevent * _bufferevent;

	/* TODO (future): protected: */
		virtual void _readcb_handler()
			{ /* to be overridden by derived bufferevent class */ }

		virtual void _writecb_handler()
			{ /* to be overridden by derived bufferevent class */ }

		virtual void _errorcb_handler(short)
			{ /* to be overridden by derived bufferevent class */ }

	/* TODO (future): private: */
		static void _readcb_wrapper(bufferevent *, void * arg)
		{
			EvBufferEventInternal * bufferevent_self = reinterpret_cast< EvBufferEventInternal * >(arg);

			bufferevent_self->_readcb_handler();
		}

		static void _writecb_wrapper(bufferevent *, void * arg)
		{
			EvBufferEventInternal * bufferevent_self = reinterpret_cast< EvBufferEventInternal * >(arg);

			bufferevent_self->_writecb_handler();
		}

		static void _errorcb_wrapper(bufferevent *, short what, void * arg)
		{
			EvBufferEventInternal * bufferevent_self = reinterpret_cast< EvBufferEventInternal * >(arg);

			bufferevent_self->_errorcb_handler(what);
		}

};

#endif // _EV_BUFFER_EVENT_INTERNAL__HPP_

// vim: set filetype=cpp :
