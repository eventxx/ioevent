# Example event-driven state machine
# Public domain - Damien Miller <djm@mindrot.org> 2007-03-26

# $Id: example2.fsm,v 1.5 2007/03/26 04:02:37 djm Exp $

# -------------------------------------------------------------------
# Comments are delimited with a '#' character and may start anywhere
# on a line. Blank lines are ignored.

# Define the enumeration and opaque struct types the state machine will use
state-enum-type mystate
event-enum-type myevent
fsm-struct-type myfsm

# Define the names of the exported API functions
current-state-function myfsm_current
initialise-function myfsm_init
free-function myfsm_free
advance-function myfsm_advance
state-enum-to-string-function myfsm_state_ntop
event-enum-to-string-function myfsm_event_ntop

# Specify what arguments we want to pass to the transition preconditions
# and callbacks
precondition-function-args event,new-state,ctx
transition-function-args ctx,event,old-state
event-callback-args ctx,event
event-precondition-args event,old-state,new-state,ctx

# Define some states and events that trigger transitions between them
state STATE_INIT
	initial-state
	on-event HELO -> STATE_HELO

state STATE_HELO
	on-event MAIL -> STATE_MAIL
	on-event QUIT -> STATE_QUIT
	onentry-func helo_enter

state STATE_MAIL
	on-event RCPT -> STATE_RCPT
	on-event QUIT -> STATE_QUIT
	onentry-func mail_enter

state STATE_RCPT
	on-event DATA -> STATE_DATA
	on-event QUIT -> STATE_QUIT
	onentry-func rcpt_enter

state STATE_DATA
	on-event END_DATA -> STATE_MAIL
	onentry-func data_enter

state STATE_QUIT
	onentry-func quit_enter

# Fill in some more details for the events, e.g. callbacks
# This section is optional; events without extra annotations
# are still created, but won't have callbacks or preconditions
# attached.
event QUIT
	event-callback quit_callback

# -------------------------------------------------------------------

