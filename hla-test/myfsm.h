/*
 * Based on output of the cfsm FSM compiler:
 * http://www.mindrot.org/projects/cfsm/
 */

#ifndef _MYFSM_H
#define _MYFSM_H

#include <sys/types.h>

/* FSM structure */
struct myfsm {
	int current_state;
};


/*
 * Allocate a new FSM and set its starting state to STATE_INIT (0)
 */
#define MYFSM_INIT()    ( (struct myfsm *)(calloc(1, sizeof(struct myfsm))) )


/*
 * Free a FSM created with MYFSM_INIT
 */
#define MYFSM_FREE(fsm) { free(fsm); }


/*
 * Returns the current state of the FSM.
 */
#define MYFSM_CURRENT(fsm) ( fsm->current_state )


#endif /* _MYFSM_H */
