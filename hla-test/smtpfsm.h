/*
 * Based on output of the cfsm FSM compiler:
 * http://www.mindrot.org/projects/cfsm/
 */

#ifndef _SMTPFSM_H
#define _SMTPFSM_H

#include "myfsm.h"

/*
 * The valid states of the FSM
 */
enum mystate {
	STATE_INIT,
	STATE_HELO,
	STATE_MAIL,
	STATE_RCPT,
	STATE_DATA,
	STATE_QUIT,
};

/*
 * Events that may cause state transitions in the FSM
 */
enum myevent {
	HELO,
	MAIL,
	QUIT,
	RCPT,
	DATA,
	END_DATA,
};


#endif /* _SMTPFSM_H */
