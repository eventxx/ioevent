/*
 * Automatically generated using the cfsm FSM compiler:
 * http://www.mindrot.org/projects/cfsm/
 */

#ifndef _FSM_H
#define _FSM_H

#include <sys/types.h>

/*
 * The FSM object itself. This is intentionally opaque - all access must be
 * through this API
 */
struct myfsm;

/*
 * The valid states of the FSM
 */
enum mystate {
	STATE_INIT,
	STATE_HELO,
	STATE_MAIL,
	STATE_RCPT,
	STATE_DATA,
	STATE_QUIT,
};

/*
 * Events that may cause state transitions in the FSM
 */
enum myevent {
	HELO,
	MAIL,
	QUIT,
	RCPT,
	DATA,
	END_DATA,
};

/*
 * Possible error return values
 */
#ifndef CFSM_OK
# define CFSM_OK			0
# define CFSM_ERR_INVALID_STATE		-1
# define CFSM_ERR_INVALID_EVENT		-2
# define CFSM_ERR_INVALID_TRANSITION	-3
# define CFSM_ERR_PRECONDITION		-4
#endif /* CFSM_OK */

/*
 * Allocate a new FSM and set its starting state to STATE_INIT
 * Will return a pointer to the opaque FSM structure on success or 
 * NULL on failure. If "errbuf" is not NULL, upto "errlen" bytes of
 * error message will be copied into "errbuf" on failure.
 */
struct myfsm *myfsm_init(char *errbuf, size_t errlen);

/*
 * Free a FSM created with %(init_func)s()
 */
void myfsm_free(struct myfsm *fsm);

/*
 * Execute a pre-defined event on the FSM that may trigger a transition.
 * The "ctx" argument is a caller-specified context pointer that
 * may be used to pass additional state to precondition, event and transition
 * callback functions.
 *
 * Will return CFSM_OK on success or one of the CFSM_ERR_* codes on failure.
 * If "errbuf" is not NULL, upto "errlen" bytes of error message will be 
 * copied into "errbuf" on failure.
 */
int myfsm_advance(struct myfsm *fsm, enum myevent ev,
    void *ctx, char *errbuf, size_t errlen);

/*
 * Convert from the %(event_enum)s enumeration to a string. Will return
 * NULL if the event is not known.
 */
const char *myfsm_event_ntop(enum myevent);

/*
 * "Safe" version of %(event_enum_to_string_func)s. Will return the string
 * "[INVALID]" if the event is not known, so it can be used directly
 * in printf() statements, etc.
 */
const char *myfsm_event_ntop_safe(enum myevent);

/*
 * Convert from the mystate enumeration to a string. Will return
 * NULL if the state is not known.
 */
const char *myfsm_state_ntop(enum mystate n);

/*
 * "Safe" version of myfsm_state_ntop(). Will return the string
 * "[INVALID]" if the state is not known, so it can be used directly
 * in printf() statements or other contexts where a NULL may be harmful.
 */
const char *myfsm_state_ntop_safe(enum mystate n);

/*
 * Returns the current state of the FSM.
 */
enum mystate myfsm_current(struct myfsm *fsm);

#endif /* _FSM_H */
