
#include <eventxx>

#include <EvBufferEvent.hpp>

#include <evhttp.h>

#include <iostream>

extern "C" {
#include "smtpfsm.h"
}

/**
enum {
	STATE_INIT = 0,
	STATE_DATA
};
**/

struct cc {
	//int state;
	struct myfsm * fsm;

	EvBufferEvent * be;
	struct bufferevent * bev;

	const char * cur_line;

};

#define MYERR_LEN 255

char myerr[MYERR_LEN + 1];

extern "C" void helo_enter(enum myevent ev, enum mystate old_state, void *ctx)
{
	struct cc * c = (struct cc *)ctx;

	bufferevent * bev = c->bev;
	evbuffer * b = ::evbuffer_new();

	const char * line = c->cur_line;

	::evbuffer_add_printf(b, "250 Hello %s\r\n", line + 5);
	::bufferevent_write_buffer(bev, b);

	::evbuffer_free(b);
}

extern "C" void react_250_ok(struct cc * c)
{
	bufferevent * bev = c->bev;
	evbuffer * b = ::evbuffer_new();

	::evbuffer_add_printf(b, "250 Ok\r\n");
	::bufferevent_write_buffer(bev, b);

	::evbuffer_free(b);
}

extern "C" void mail_enter(enum myevent ev, enum mystate old_state, void *ctx)
{
	struct cc * c = (struct cc *)ctx;

	react_250_ok(c);
}

extern "C" void rcpt_enter(enum myevent ev, enum mystate old_state, void *ctx)
{
	struct cc * c = (struct cc *)ctx;

	react_250_ok(c);
}

extern "C" void data_enter(enum myevent ev, enum mystate old_state, void *ctx)
{
	struct cc * c = (struct cc *)ctx;

	bufferevent * bev = c->bev;
	evbuffer * b = ::evbuffer_new();

	::evbuffer_add_printf(b, "354 End data with <CR><LF>.<CR><LF>\r\n");
	::bufferevent_write_buffer(bev, b);

	::evbuffer_free(b);
}

extern "C" void quit_callback(enum myevent ev, void *ctx)
{
	struct cc * c = (struct cc *)ctx;

	bufferevent * bev = c->bev;
	evbuffer * b = ::evbuffer_new();

	::evbuffer_add_printf(b, "221 Bye\r\n");
	::bufferevent_write_buffer(bev, b);

	::evbuffer_free(b);
}

extern "C" void quit_enter(enum myevent ev, enum mystate old_state, void *ctx)
{

}

void readcb(struct bufferevent *bev, void * v)
{
	char * line;

	struct cc * c = (struct cc *)v;

	while (line = ::evbuffer_readline(bev->input))
	{
		c->cur_line = line;

		//if (c->state == STATE_INIT)
		if (myfsm_current(c->fsm) != DATA)
		{
			if (!strncmp(line, "HELO ", 5) ||
			    !strncmp(line, "EHLO ", 5))
			{
				//react_helo(c);
				myfsm_advance(c->fsm, HELO, c, myerr, MYERR_LEN);
			}
			else
			if (!strncmp(line, "MAIL", 4))
			{
				//react_250_ok(c);
				myfsm_advance(c->fsm, MAIL, c, myerr, MYERR_LEN);
			}
			else
			if (!strncmp(line, "RCPT", 4))
			{
				//react_250_ok(c);
				myfsm_advance(c->fsm, RCPT, c, myerr, MYERR_LEN);
			}
			else
			if (!strncmp(line, "DATA", 4))
			{
				//react_data(c);
				myfsm_advance(c->fsm, DATA, c, myerr, MYERR_LEN);
			}
			else
			if (!strncmp(line, "QUIT", 4))
			{
				//react_bye(c);
				myfsm_advance(c->fsm, QUIT, c, myerr, MYERR_LEN);
			}
		}
		else
		{
			if (!strcmp(line, "."))
			{
				//react_data_line(c);
				//myfsm_advance(c->fsm, END_DATA, c, myerr, sizeof myerr - 1);
				myfsm_advance(c->fsm, END_DATA, c, myerr, sizeof(myerr) - 1);
			}
		}

		free(line);
		c->cur_line = NULL;
	}

}

void writecb(struct bufferevent *, void *)
{

}

void errorcb(struct bufferevent *, short, void *)
{

}

void accept_handler(int s1, short, void *)
{
	struct cc * c = new cc;
	//c->state = STATE_INIT;

	c->fsm = myfsm_init(NULL, 0);

	EvBufferEvent * be = new EvBufferEvent(readcb, writecb, errorcb, c);

	be->accept(s1);

	c->be = be;
	c->bev = be->_bufferevent;

	c->cur_line = NULL;

	evbuffer * b = ::evbuffer_new();

	::bufferevent_enable(be->_bufferevent, EV_READ);

	be->write("220 www.example.com ESMTP postfix\r\n", 35);

}

main()
{
	::event_init();

	int sock = ::bind_socket(NULL, 5525);

	::bufev_socket_listen(sock, 10);

	eventxx::cevent accept_event(sock, eventxx::READ | eventxx::PERSIST, &accept_handler, NULL);
	::event_add(&accept_event, NULL);

	::event_dispatch();

}
