/*
 * Automatically generated using the cfsm FSM compiler:
 * http://www.mindrot.org/projects/cfsm/
 */

#include <sys/types.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "smtpfsm.h"

/* Private view of opaque FSM structure */
struct myfsm {
	enum mystate current_state;
};

/* Prototypes for state transition entry callbacks */
void helo_enter(enum myevent ev, enum mystate old_state, void *ctx);
void mail_enter(enum myevent ev, enum mystate old_state, void *ctx);
void rcpt_enter(enum myevent ev, enum mystate old_state, void *ctx);
void data_enter(enum myevent ev, enum mystate old_state, void *ctx);
void quit_enter(enum myevent ev, enum mystate old_state, void *ctx);

/* Prototypes for event callback functions */
void quit_callback(enum myevent ev, void *ctx);

static int
_is_mystate_valid(enum mystate n)
{
	if (!(n >= STATE_INIT && n <= STATE_QUIT))
		return -1;
	return 0;
}

const char *
myfsm_state_ntop(enum mystate n)
{
	const char *state_names[] = {
		"STATE_INIT",
		"STATE_HELO",
		"STATE_MAIL",
		"STATE_RCPT",
		"STATE_DATA",
		"STATE_QUIT",
	};

	if (_is_mystate_valid(n) != 0)
		return NULL;
	return state_names[n];
}

const char *
myfsm_state_ntop_safe(enum mystate n)
{
	const char *r = myfsm_state_ntop(n);

	return r == NULL ? "[INVALID]" : r;
}

static int
_is_myevent_valid(enum myevent n)
{
	if (!(n >= HELO && n <= END_DATA))
		return -1;
	return 0;
}

const char *
myfsm_event_ntop(enum myevent n)
{
	const char *event_names[] = {
		"HELO",
		"MAIL",
		"QUIT",
		"RCPT",
		"DATA",
		"END_DATA",
	};

	if (_is_myevent_valid(n) != 0)
		return NULL;
	return event_names[n];
}

const char *
myfsm_event_ntop_safe(enum myevent n)
{
	const char *r = myfsm_event_ntop(n);

	return r == NULL ? "[INVALID]" : r;
}

struct myfsm *
myfsm_init(char *errbuf, size_t errlen)
{
	struct myfsm *ret = NULL;

	if ((ret = calloc(1, sizeof(*ret))) == NULL) {
		if (errlen > 0 && errbuf != NULL)
			snprintf(errbuf, errlen, "Memory allocation failed");
		return NULL;
	}
	ret->current_state = STATE_INIT;
	return ret;
}

void
myfsm_free(struct myfsm *fsm)
{
	memset(fsm, '\0', sizeof(fsm));
	free(fsm);
}

enum mystate
myfsm_current(struct myfsm *fsm)
{
	return fsm->current_state;
}

int myfsm_advance(struct myfsm *fsm, enum myevent ev,
    void *ctx, char *errbuf, size_t errlen)
{
	enum mystate old_state = fsm->current_state;
	enum mystate new_state;

	/* Sanity check states */
	if (_is_mystate_valid(fsm->current_state) != 0) {
		if (errlen > 0 && errbuf != NULL) {
			snprintf(errbuf, errlen, "Invalid current_state (%d)",
			    fsm->current_state);
		}
		return CFSM_ERR_INVALID_STATE;
	}
	if (_is_myevent_valid(ev) != 0) {
		if (errlen > 0 && errbuf != NULL)
			snprintf(errbuf, errlen, "Invalid event (%d)", ev);
		return CFSM_ERR_INVALID_EVENT;
	}


	/* Event validity checks */
	switch(old_state) {
	case STATE_INIT:
		switch (ev) {
		case HELO:
			new_state = STATE_HELO;
			break;
		default:
			goto bad_event;
		}
		break;
	case STATE_HELO:
		switch (ev) {
		case MAIL:
			new_state = STATE_MAIL;
			break;
		case QUIT:
			new_state = STATE_QUIT;
			break;
		default:
			goto bad_event;
		}
		break;
	case STATE_MAIL:
		switch (ev) {
		case RCPT:
			new_state = STATE_RCPT;
			break;
		case QUIT:
			new_state = STATE_QUIT;
			break;
		default:
			goto bad_event;
		}
		break;
	case STATE_RCPT:
		switch (ev) {
		case DATA:
			new_state = STATE_DATA;
			break;
		case QUIT:
			new_state = STATE_QUIT;
			break;
		default:
			goto bad_event;
		}
		break;
	case STATE_DATA:
		switch (ev) {
		case END_DATA:
			new_state = STATE_MAIL;
			break;
		default:
			goto bad_event;
		}
		break;
	case STATE_QUIT:
		goto bad_event;
	}

	/* Event callbacks */
	switch(ev) {
	case QUIT:
		quit_callback(ev, ctx);
		break;
	default:
		break;
	}

	/* Switch state now */
	fsm->current_state = new_state;

	/* New state entry callbacks */
	switch(new_state) {
	case STATE_HELO:
		helo_enter(ev, old_state, ctx);
		break;
	case STATE_MAIL:
		mail_enter(ev, old_state, ctx);
		break;
	case STATE_RCPT:
		rcpt_enter(ev, old_state, ctx);
		break;
	case STATE_DATA:
		data_enter(ev, old_state, ctx);
		break;
	case STATE_QUIT:
		quit_enter(ev, old_state, ctx);
		break;
	default:
		break;
	}

	return CFSM_OK;

 bad_event:
	if (errlen > 0 && errbuf != NULL) {
		snprintf(errbuf, errlen,
		    "Invalid event %s in state %s",
		    myfsm_event_ntop_safe(ev),
		    myfsm_state_ntop_safe(fsm->current_state));
	}
	return CFSM_ERR_INVALID_TRANSITION;
}
